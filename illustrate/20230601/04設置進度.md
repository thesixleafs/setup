設置進度
===

**1. 標題**
---

用於設定 **`進度條`** 上的文字。

<img src="https://gitlab.com/thesixleafs/setup/-/raw/main/img/20230601/11.gif" width="40%">

**2. 字型 & 文字大小**
---

1. **`字型`**： 設定文字的字體。

2. **`文字大小`**： 調整 **`進度條`** 上的文字大小。

<img src="https://gitlab.com/thesixleafs/setup/-/raw/main/img/20230601/12.gif" width="40%">

**3. 圖型寬度**
---

調整 **`進度條`** 的高度。

<img src="https://gitlab.com/thesixleafs/setup/-/raw/main/img/20230601/13.gif" width="40%">

**4. 字框**
---

1. 勾選後 **`進度條`** 上的文字會擁有文字框。

2. 顏色可自行調整。

3. 光暈部分可看狀況勾選。

<img src="https://gitlab.com/thesixleafs/setup/-/raw/main/img/20230601/14-1.gif" width="40%">

**5. 文字顏色 & 圖型顏色**
---

可設定多種顏色，可依照需求設定 1 種或 1 種以上的顏色。

輸入的格式如下：

**`#FF00FF ,`**

請注意輸入色碼後要加上 **`,`** 。

<img src="https://gitlab.com/thesixleafs/setup/-/raw/main/img/20230601/15.gif" width="40%">

**6. 現在金額 & 目標金額**
---

設定 **`進度條`** 上的數字顯示部分。

<img src="https://gitlab.com/thesixleafs/setup/-/raw/main/img/20230601/16.gif" width="40%">

**7. 手動加減**
---

如果資料突然沒有收到，可以用此功能增減金額， **`進度條`** 會即時增減。

<img src="https://gitlab.com/thesixleafs/setup/-/raw/main/img/20230601/17.gif" width="40%">
