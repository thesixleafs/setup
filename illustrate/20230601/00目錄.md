說明目錄
===

<a href="https://gitlab.com/thesixleafs/setup/-/blob/main/illustrate/20230601/01%E4%B8%8B%E8%BC%89.md" target="_blank">**1. 下載**</a>
---

<a href="https://gitlab.com/thesixleafs/setup/-/blob/main/illustrate/20230601/02%E4%B8%BB%E6%8E%A7%E5%8F%B0.md" target="_blank">**2. 主控台**</a>
---

<a href="https://gitlab.com/thesixleafs/setup/-/blob/main/illustrate/20230601/03%E9%A1%8F%E8%89%B2%E8%A8%AD%E5%AE%9A.md" target="_blank">**3. 顏色設定**</a>
---

<a href="https://gitlab.com/thesixleafs/setup/-/blob/main/illustrate/20230601/04%E8%A8%AD%E7%BD%AE%E9%80%B2%E5%BA%A6.md" target="_blank">**4. 設置進度**</a>
---

<a href="https://gitlab.com/thesixleafs/setup/-/blob/main/illustrate/20230601/05%E5%8A%A0%E7%8F%AD%E8%A8%88%E6%99%82.md" target="_blank">**5. 加班計時**</a>
---

<a href="https://gitlab.com/thesixleafs/setup/-/blob/main/illustrate/20230601/06%E6%97%A9%E5%AE%89%E8%A8%88%E7%AE%97.md" target="_blank">**6. 早安計算**</a>
---
